package com.example.demo.student;


import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfig {
	
	@Bean
	CommandLineRunner commandLineRunner(StudentRepository repository) {
		return args ->{
		Student vuk =new Student("Vuk",
				"vuk01m@gmail.com",
				LocalDate.of(2001, Month.JULY, 3));
			
		Student zoran =	new Student("Zoran",
				"zoran94m@gmail.com",
				LocalDate.of(1994, Month.JUNE, 24));
			
			
			
		repository.saveAll(List.of(vuk,zoran));
			
			
		};
		
	}

}
