package com.example.demo.student;



import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentService {
 
	private final StudentRepository studentReposetory;
	
	
	@Autowired
	public StudentService(StudentRepository studentReposetory) {
		
		this.studentReposetory = studentReposetory;
	}



	public List <Student> hello() {
		return studentReposetory.findAll();
	}



	public void insertNewUser(Student student) {
		Optional<Student> isOptional= studentReposetory.findStudentByEmail(student.getEmail());
		if(isOptional.isPresent()) {
			
			throw new  IllegalStateException("Email Taken");
		}
	
		studentReposetory.save(student);
	
	
	
	}



	public void deleteStudent(Long studentId){
	boolean exists = studentReposetory.existsById(studentId);
	
	if(!exists) {
		
		throw new IllegalStateException("Student with id="+studentId+" does not exists");
		
	}
	
	studentReposetory.deleteById(studentId);
		
	}


@Transactional
	public void updateStudent(Long studentId, String name, String email) {
		Student student = studentReposetory.findById(studentId).orElseThrow(()->
		new IllegalStateException("Student with this id does not exists"));
		
		if(name != null &&
				name.length()>0 &&
				!Objects.equals(student.getName(), name)) {
			student.setName(name);
		}
		
		if(email != null &&
				email.length()>0 &&
				!Objects.equals(student.getEmail(), email)) {
			Optional<Student> optional = studentReposetory.findStudentByEmail(email);
			if(optional.isPresent()) {
				throw new IllegalStateException("Email Taken");
			}
				
			
			student.setEmail(email);
		}
		
	}
	
	
}
